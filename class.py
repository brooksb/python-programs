class Cat:
    def __init__(self, name, age) -> None:
        self.name = name
        self.age = age

    def meow(self):
        return 'meow'

    def scratch(self):
        return 'scratch'

    def oldest_cat(self):
        age = 0
        if self.age > age:
            age = self.age
        return self.name, age


arnold = Cat('arnold', 1)
fire = Cat('fire', 20)
green = Cat('green', 420)
print(fire.scratch())


class Dog:
    dog_id = 1

    def __init__(self, name, age=0) -> None:
        self.name = name
        self.age = age
        self.id = Dog.dog_id
        Dog.dog_id += 1

    def bark(self):
        return f"{self.name} says woof!"

    def __str__(self) -> str:
        return f"Dog {self.id} named {self.name} is {self.age} years old"

# decorator
    @classmethod
    def get_total_dogs(cls):
        return cls.dog_id - 1


d1 = Dog('Spock', 5)
d2 = Dog('Sniffles', 2)
d3 = Dog('Jack', 6)
d4 = Dog("Moon", 9)

print(Dog.get_total_dogs())
