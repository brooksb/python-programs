# 1. Reverse a list
# Given:
list1 = [100, 200, 300, 400, 500]
# Expected output:
# [500, 400, 300, 200, 100]
list1.reverse()
print(list1)


# 2. Turn every item of a list into its square
# Given:
list2 = [1, 2, 3, 4, 5, 6, 7]
# Expected output:
# [1, 4, 9, 16, 25, 36, 49]
for nums in list2:
    print(nums ** 2)


# 3. Remove empty strings from the list
# Hint: challenge: you could try using .filter here!
# Given:
list3 = ["Mike", "", "Emma", "Aisha", "", "Dalante"]
# Expected output:
# ["Mike", "Emma", "Aisha", "Dalante"]
while "" in list3:
    list3.remove("")
    print(list3)


# 4. Add new item to a list after a specified item
# Task: Add item 7000 after 6000 in the following list
# Given:
list4 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
#        0    1               2                  3   4
#               #  0    1        2         3
#                            # 0     1
# Expected output:
# [10, 20, [300, 400, [5000, 6000, 7000], 500], 30, 40]
list4[2][2].append(7000)
print(list4)


# Write a for loop so that every item in the list is printed
animals = [
    "koala", "cat", "fox", "panda", "chipmunk",
    "sloth", "penguin", "dolphin"]
for name in animals:
    print(name)


# Write a for loop which print "Hello!, "
# plus each name in list. i.e.: "Hello!, Sam"
names = ["Sam", "Lisa", "Micha", "Fatima", "Wyatt", "Emma", "Sage"]
for person in names:
    print('Hello!', person)


# Write a for loop that iterates through a string and prints every letter.
str = "Antarctica"
for letter in str:
    print(letter)


# Using a for loop and .append() method append each
# item with a Dr. prefix to the lst.
lst1 = ["Phil", "Oz", "Seuss", "Dre"]
lst2 = []

# Your code
for nam in lst1:
    lst2.append('Dr.' + nam)
print(lst2)


# Write a for loop which appends the square of each number to the new list.
lst6 = [3, 7, 6, 8, 9, 11, 15, 25]
lst7 = []

# Your code
for num in lst6:
    lst7.append(num ** 2)
print(lst7)


# Write a for loop using an if statement, that appends
# each number to the new list if it's positive.
lst10 = [111, 32, -9, -45, -17, 9, 85, -10]
lst20 = []

# Type your answer here.
for num in lst10:
    if num > 1:
        lst20.append(num)
    else:
        None
print(lst20)
