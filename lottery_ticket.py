customer_age = 23
customer_interest = True


def verify_age():
    if customer_age > 17:
        print("You can buy a lottery ticket.")
        print("How many would you like?")
    else:
        print("You may not buy a lottery ticket.")
        print("Can I interest you in some candy?")
    print("Thank you for your patronage.")


def verify_interest():
    if customer_interest is True:
        print("Excellent choice, but we only have Twix.")
        print("How many would you like? ")
    else:
        print("Come back another time!")
    print("Thank you for your patronage.")


def verified_age(age):
    if age > 17:
        print(f"{age} is old enough to buy lottery ticket.")
        # print("How many would you like?")
    else:
        print(f"{age} is not old enough to buy a lottery ticket.")
        # print("Can I interest you in some candy?")
    print("Thank you for your patronage.")


customer_one_age = 23
# interest_in_candy = True
# verify_interest()
