# Create a Car class that has the required state of:
# make, a string
# model, a string
# year, an integer
# in that order.
# Create a __str__ method on the class that
# returns the year, make, and model separated by spaces.
# Example values:
#   make:  Oldsmobile
#   model: Alero
#   year:  2001

# __str__ would return "2001 Oldsmobile Alero"

class Car:
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year

    def __str__(self):
        return f"{self.year} {self.make} {self.model}"
