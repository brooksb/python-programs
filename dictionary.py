# Python collections/containers
# lists [] mutable
# tuples () immutable
# dictionaries {} mutable

adventurer1 = {
    'name': 'Bro',
    'hit_points': 10,
    'belongings': ['sword', 'magic potion', 'tums', 'water bottle'],
    'companion': {
        'name': 'Velma',
        'type': 'parasite',
        'belongings': [
            'scuba tank', 'ipod', 'rubber duck', 'toilet paper',
            'health insurance', 'glasses']
    }
}

print(adventurer1.get('hit_points'))

things = {
    'book': 'Dune',
    'tea': 'oolong',
    'computer': 'macbook pros'
}
for key, value in things.items():
    print(key, 'is the', value)


# Practice Problems

# 1. Covert two lists into a dictionary
keys = ['ten', 'twenty', 'thirty']
values = [10, 20, 30]
# expected output
# {
#     'ten': 10,
#     'twenty': 20,
#     'thirty': 30
# }
# d1 = dict(zip(keys, values))
# print(d1)
dictionary = {}
for k in keys:
    for v in values:
        dictionary[k] = v
        values.remove(v)
        break
print(dictionary)


# 2. Delete a list of keys from a dict
person = {
    'name': 'Stephanie',
    'age': 450,
    'transportation': 'bus',
    'city': 'sf'
}
# list of keys to delete are age, city
person.pop('age')
person.pop('city')


# 3. Rename transportation to super power
person.update({'super power': 'bus'})
print(person)


# 4. Merge two dictionaries into one


def Merge(dic1, dic2):
    res = dic1 | dic2
    return res


dic1 = {
    'ten': 10,
    'twenty': 20,
    'thirty': 30
}

dic2 = {
    'forty': 40,
    'fifty': 50,
    'sixty': 60
}
dic3 = Merge(dic1, dic2)
print(dic3)


# 5. Define what a dictionary is in your on words. Be as specific as possible.
# Tell us what you can do with them, and how you think they can be used
# a dictionary is a mutable list contained in {} and contains key:value


# 6. Define five methods you can apply to dictionaries and explain what they do
# update- updates dictionary
# clear- clears dictionary
# pop-removes specific key
# popitem- removes most recent item
# values- returns all values


d9 = {
  "Asia": ["China", "Mongolia", "India"],
  "South America": ["Brazil", "Argentina", "Chile"],
  "North America": ["United States", "Canada", "Mexico"], "Antarctica": [],
  "Africa": ["South Africa", "Algeria", "Kenya", "Ethiopia", "Egypt"],
  "Europe": ["France", "Germany", "England", "Spain", "Greece", "Italy"],
  "Australia": ["New Zealand", "Fiji", "Australia"]
  }
print(d9.get('Asia'))


employee = {
    "first_name": "W.L.",
    "last_name": "McCrea",
    "company": "Voltage Ad",
    "company_location": "Louisville, Kentucky",
    "years_experience": 4,
    "role": "graphic designer"
}
# "The candidate's name is W.L. McCrea. She has
# 4 years of experience as a graphic designer
# at Voltage Ad in Louisville, Kentucky"


text = ["s", "a", "j", "q", "l", "m", "v", "l", "t", "h", "b", "w",
        "r", "g", "n", "c", "y", "i", "z", "a", "l", "t", "x", "e",
        "k", "o", "r", "u", "p", "l", "n", "c", "d", "q", "l", "w"]
dict = {}

for t in text:
    dict[t] = text.count(t)
print(dict)

maximum = 0

for v in dict.values():
    if v > maximum:
        maximum = v
print(maximum)

for k, v in dict.items():
    if maximum == v:
        print(k)


# Task: Loop through the dictionary and create a new one where the values are
# the squares of the current values. Print the new dictionary.
my_dict = {'a': 1, 'b': 2, 'c': 3}
# Expected output:
# {'a': 1, 'b': 4, 'c': 9}
new_dict = {}

for k in my_dict:
    value = my_dict[k]
    new_dict[k] = value ** 2
print(new_dict)


menu = [
    {
        "item": "Bubble tea",
        "quantity": 6,
        "price_per_unit": 5.00
    },
    {
        "item": "Unicorn onesie",
        "price_per_unit": 49.99
    },
    {
        "item": "Legal briefcase",
        "quantity": 1,
        "price_per_unit": 149.99
    },
    {
        "item": "Fake mustaches",
        "quantity": 10,
        "price_per_unit": 1.00,
    }
]


# Task: Write some code that will print a list of each of the products
# Expected output:
# ["Bubble tea", "Unicorn onesie", "Legal briefcase", "Fake mustaches"]


# Task:write some code that will print a new list of dictionaries showing each
# product and the total price of each product (ie quantity x price_per_unit).
# If the quantity is absent, total price should be 0
# Expected output:
# {
#   'Bubble tea': 30.0,
#   'Unicorn onesie': 0.0,
#   'Legal briefcase': 149.99,
#   'Fake mustaches': 10.0
# }
