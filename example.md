Success is counted sweetest
By those who ne'er succeed.
To comprehend a nectar
Requires sorest need.

Not one of all the Purple Host
Who took the Flag today
Can tell the definition
So clear of Victory
