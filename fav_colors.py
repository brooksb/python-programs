def create_player_name(name):
    player_name = name.lower()
    player_name = player_name.replace('', '-')
    return player_name


player1_name = create_player_name('')


def get_num_colors():
    num_colors_str = input("How many fav colors do you have? ")
    num_colors = int(num_colors_str)
    return num_colors


print("Hi, I'd like to ask you about your fav colors.")

# Here's the new line that replaced the old two lines
num_colors = get_num_colors()

print("Thanks! I will now ask you for each of those.")

favorite_colors = []

for color_number in range(num_colors):
    num = str(color_number + 1)

    prompt = "What is your #" + num + " fav color? "

    color = input(prompt)

    favorite_colors.append(color)

sorted_colors = sorted(favorite_colors)

print("Thank you! I have your fav colors as:")

for color in sorted_colors:
    print("  ", color)


# With renamed parameter and variables
def get_color(fav_number):
    num = str(fav_number + 1)
    prompt = "What is your #" + num + " fav color? "
    answer = input(prompt)
    return answer


# With renamed variables
def get_num_colors():
    response = input("How many fav colors do you have? ")
    response_num = int(response)
    return response_num


print("Hi, I'd like to ask you about your fav colors.")
num_colors = get_num_colors()
print("Thanks! I will now ask you for each of those.")
favorite_colors = []

for color_number in range(num_colors):
    color = get_color(color_number)
    favorite_colors.append(color)

sorted_colors = sorted(favorite_colors)
print("Thank you! I have your fav colors as:")
for color in sorted_colors:
    print("  ", color)
