def two_sums(nums, target):
    sorted_list = sorted(nums)
    print(sorted_list)
    start_index = 0
    end_index = len(nums) - 1

    while start_index < end_index:
        current_sum = sorted_list[start_index] + sorted_list[end_index]
        if current_sum == target:
            return (sorted_list[start_index], sorted_list[end_index])
        elif current_sum > target:
            end_index -= 1
        else:
            start_index += 1


target_sum = 11
num_list = [22, 9, 2, 43, 2, 1, 8, 7, 3]

print(two_sums(num_list, target_sum))  # returns tuple
