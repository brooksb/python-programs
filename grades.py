scores = [70, 70, 70, 58, 92, 75, 30, 87]
total = 0
for score in scores:
    total += score
average = total / len(scores)
if (average >= 90):
    print('A')
elif (average >= 80 and average < 90):
    print('B')
elif (average >= 70 and average < 80):
    print('C')
elif (average >= 60 and average < 70):
    print('D')
else:
    print('F')
