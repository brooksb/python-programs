# Complete the function make_description below, which takes a person's name and
# a dictionary of their attributes and constructs a single string from them.
# For example, say the person's name is "Lulu" and their attribute
# dictionary contained the following entries:
# {
#     "hair": "red",
#     "eyes": "blue",
# }
# Then, the return value of the function should be the string:
# Lulu, red hair, blue eyes
# If the dictionary has no entries, then the return value should
# just be the person's name.

data = {
    "hair": "red",
    "eyes": "blue",
}


def make_description(name, attributes):
    description = name
    for key, value in attributes.items():
        description += f", {value} {key}"
    return description


print(make_description("lulu", data))


def reverse_entries(dictionary):
    new_d = {}
    for k, v in dictionary.items():
        new_d[v] = k
    return new_d


def reverse(dict):
    return {value: key for key, value in dict.items()}


# Complete the function horizontal_bar_chart below that takes in a string and creates a horizontal bar chart made out of the instances of the letters in that string based on the number of each letter.
# The return value is a list of the strings of the letters in alphabetical order.
# For example, given the sentence "abba has a banana"
#  it would return this list of strings because there are seven "a", three "b", one "h", two "n", and one "s" letters in it:
# [
#   "aaaaaaa",
#   "bbb",
#   "h",
#   "nn",
#   "s",
# ]
# All letters in the input will be lowercase letters.
# You may want to look at the methods for dictionaries and the built-in functions. Remember, you can also compare strings with >= and <=.


def horizontal_bar_chart(sentence):
    freq_dict = {}
    for char in sentence:
        if char >= "a" and char <= "z":
            if char not in freq_dict:
                freq_dict[char] = ""
            freq_dict[char] += char
    result = []
    for string in freq_dict.values():
        result.append(string)
    return list(sorted(result))
