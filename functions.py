age_string = input("How old are you?")
age = int(age_string)
print("Next year, you'll be", age + 1, "years old")
# print(1, 2, 3, 4, 5, "and that's it!")


# Print a greeting
print("Hi, I'd like to ask you about your fav colors.")

# Ask for input
num_colors_str = input("How many fav colors do you have? ")

# Turn the str input into an int
num_colors = int(num_colors_str)

# Print a thanks
print("Thanks! I will now ask you for each of those.")

# Create a list to put the responses in
favorite_colors = []

# Loop the number of times equal to their number
# of favorite colors
for color_number in range(num_colors):
    # Remember that range creates a list starting
    # at 0, so we add 1 to make it more readable
    # for humans
    num = str(color_number + 1)

    # Create the string prompt that will ask them
    # for their favorite color
    prompt = "What is your #" + num + " fav color? "

    # Prompt them for their input
    color = input(prompt)

    # Add the color to the list of favorite colors
    favorite_colors.append(color)

# Sort the colors into a new list
sorted_colors = sorted(favorite_colors)

# Print a thank you message
print("Thank you! I have your fav colors as:")

# Print each of the colors in the sorted list
for color in sorted_colors:
    print("  ", color)
