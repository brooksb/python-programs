# my_list = ["Evander", 22, True]
# print(my_list)

days_of_week = [
  "Sunday",       # Index 0
  "Monday",       # Index 1
  "Tuesday",      # Index 2
  "Wednesday",    # Index 3
  "Thursday",     # Index 4
  "Friday",       # Index 5
  "Saturday",     # Index 6
]

days_of_week[3] = "Hump day!"
days_of_week[5] = 'FunYay!'

print(days_of_week)
print("The first day of the week is", days_of_week[0])
print('The last day of the week is', days_of_week[6])


months_of_year = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]

for month_name in months_of_year:
    print(month_name)

print("Months with names that have")
print("more than eight letters:")

for month_name in months_of_year:
    if len(month_name) > 8:
        print(month_name)

codes_and_cities = {
    "LAX": "Los Angeles",
    "LGA": "New York City",
    "IAH": "Houston",
    "BOI": "Boise",
    "SEA": "Seattle",
    "GSP": "Greenville",
}
