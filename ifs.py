num = 10

if num < 3:
    num = num + 10
else:
    num = num - 5

print(num)
this_is_false = 3 < 2
print(this_is_false)

if "":
    print("Empty string is considered true")
else:
    print("Empty string is considered false")

if 0:
    print("0 is considered true")
else:
    print("0 is considered false")
a = 3
b = 3

print(id(a))
print(id(b))

a = 3.0
b = 3

print(id(a))
print(id(b))
a = "hello"
b = "hello"

print(id(a))
print(id(b))
a = [1, 2, 3, 4]
b = [1, 2, 3, 4]

print(id(a))
print(id(b))
a = (1, 2, 3, 4)
b = (1, 2, 3, 4)

print(id(a))
print(id(b))
3 == 3
bool(0)
